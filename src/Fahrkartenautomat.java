﻿import java.util.Scanner;

//Aufgabe
class Fahrkartenautomat {

	public static int anzFgesamt = 0;

	public static double fahrkartenbestellungErfassen() {

		int ticket;
		int anzF = 0;
		double ticketpreis = 0;
		double zuZahlenderBetrag = 0;
		boolean Menü = true;

		Scanner tastatur = new Scanner(System.in);

		String[] ticketart = { "1 Einzelfahrschein Berlin AB ", "2 Einzelfahrschein Berlin BC ",
				"3 Einzelfahrschein Berlin ABC ", "4 Kurzstrecke", "5 Tageskarte Berlin AB", "6 Tageskarte Berlin BC",
				"7 Tageskarte Berlin ABC", "8 Kleingruppen-Tageskarte Berlin AB", "9 Kleingruppen-Tageskarte Berlin BC",
				"10 Kleingruppen-Tageskarte Berlin ABC" };
		
		double[] preise = { 2.9, 3.3, 3.6, 1.9, 8.6, 9, 9.6, 23.5, 24.3, 24.9 };

		System.out.println("Ticketauswahl: ");

		for (int i = 0; i < ticketart.length; i++) {
			
			System.out.println(ticketart[i]);
			
		}

		ticket = tastatur.nextInt();

		ticketpreis = preise[ticket - 1];

		System.out.print("Anzahl Fahrkarten (1 - 10): ");

		anzF = tastatur.nextInt();

		if (anzF > 10 || anzF < 1) {

			System.out.println(
					"Die Anzahl der eingegebenen Fahrkarten ist nicht korrekt!\nEs wird nur eine Fahrkarte berrechnet! ");
			anzF = 1;
		}

		anzFgesamt = anzFgesamt + anzF;
		zuZahlenderBetrag = zuZahlenderBetrag + anzF * ticketpreis;

		return zuZahlenderBetrag;

	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {

		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;

		// Geldeinwurf
		// -----------
		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f%n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.println("Eingabe (mind. 5Ct, höchstens 10 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		return eingezahlterGesamtbetrag;

	}

	public static void fahrkartenAusgabe() {

		// Fahrscheinausgabe
		// -----------------
		if (anzFgesamt > 1) {
			System.out.println("\nFahrscheine werden ausgegeben");
		} else {
			System.out.println("\nFahrschein wird ausgegeben");
		}
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rückgeldAusgabe(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {

		// Rückgeldberechnung und -Ausgabe
		// -------------------------------
		double rückgabebetrag;

		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von: %.2f%s%n", rückgabebetrag, " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 10.0) // 2 EURO-Münzen
			{
				System.out.println("10 EURO");
				rückgabebetrag -= 10.0;
			}
			while (rückgabebetrag >= 5.0) // 2 EURO-Münzen
			{
				System.out.println("5 EURO");
				rückgabebetrag -= 5.0;
			}
			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}
	}

	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;

		zuZahlenderBetrag = fahrkartenbestellungErfassen();

		eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

		fahrkartenAusgabe();

		rückgeldAusgabe(zuZahlenderBetrag, eingezahlterGesamtbetrag);

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}
}